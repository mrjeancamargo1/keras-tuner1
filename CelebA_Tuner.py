'''
pip uninstall tensorflow
pip install keras==2.1.5
pip install -U keras-tuner
pip install pandas
pip install numpy
pip install matplotlib
pip install opencv-python
'''
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras
from kerastuner.tuners import RandomSearch
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import  Conv2D, MaxPooling2D, Dense, Flatten, Activation
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

plt.style.use('ggplot')
# @title Model Input
img_size = 224  # @param ["192", "224"] {type:"raw", allow-input: true}

IMG_W = img_size
IMG_H = img_size
IMG_SHAPE = (IMG_H, IMG_W, 3)
TARGET_SIZE = (IMG_H, IMG_W)


def get_annotation(fnmtxt, verbose=True):
    if verbose:
        print("_" * 70)
        print(fnmtxt)

    rfile = open(fnmtxt, 'r')
    texts = rfile.read().split("\n")
    rfile.close()

    columns = np.array(texts[1].split(" "))
    columns = columns[columns != ""]
    df = []
    for txt in texts[2:]:
        txt = np.array(txt.split(" "))
        txt = txt[txt != ""]

        df.append(txt)

    df = pd.DataFrame(df)

    if df.shape[1] == len(columns) + 1:
        columns = ["image_id"] + list(columns)
    df.columns = columns
    df = df.dropna()
    if verbose:
        print(" Total number of annotations {}\n".format(df.shape))
        print(df.head())
    ## cast to integer
    for nm in df.columns:
        if nm != "image_id":
            df[nm] = pd.to_numeric(df[nm], downcast="float")
    return (df)

class CelebA():
    '''Wraps the celebA dataset, allowing an easy way to:
         - Select the features of interest,
         - Split the dataset into 'training', 'test' or 'validation' partition.
    '''

    def __init__(self, main_folder='data/', selected_features=None, drop_features=[]):
        self.main_folder = main_folder
        self.images_folder = os.path.join(main_folder, 'img_align_celeba/')
        self.attributes_path = os.path.join(main_folder, 'list_attr_celeba.txt')
        self.partition_path = os.path.join(main_folder, 'list_eval_partition.txt')
        self.selected_features = selected_features
        self.features_name = []
        self.__prepare(drop_features)

    def __prepare(self, drop_features):
        '''do some preprocessing before using the data: e.g. feature selection'''
        # attributes:
        if self.selected_features is None:
            self.attributes = get_annotation(self.attributes_path)
            self.num_features = 40
        else:
            self.num_features = len(self.selected_features)
            self.selected_features = self.selected_features.copy()
            self.selected_features.append('image_id')
            self.attributes = get_annotation(self.attributes_path)[self.selected_features]

        # remove unwanted features:
        for feature in drop_features:
            if feature in self.attributes:
                self.attributes = self.attributes.drop(feature, axis=1)
                self.num_features -= 1

        self.attributes.set_index('image_id', inplace=True)
        self.attributes.replace(to_replace=-1, value=0, inplace=True)
        self.attributes['image_id'] = list(self.attributes.index)
        # self.attributes.drop(self.attributes.columns[-1], axis=1, inplace=True)

        self.features_name = list(self.attributes.columns)[:-1]

        # load ideal partitioning:
        self.partition = pd.read_csv(self.partition_path, sep=" ")
        self.partition.set_index('image_id', inplace=True)

    def split(self, name='0', drop_zero=False):
        '''Returns the [0 'training', 1 'validation', 2 'test'] split of the dataset'''
        # select partition split:
        if name is '0':
            to_drop = self.partition.where(lambda x: x != 0).dropna()
        elif name is '1':
            to_drop = self.partition.where(lambda x: x != 1).dropna()
        elif name is '2':  # test
            to_drop = self.partition.where(lambda x: x != 2).dropna()
        else:
            raise ValueError('CelebA.split() => `name` must be one of [0-training, 1-validation, 2-test]')

        partition = self.partition.drop(index=to_drop.index)

        # join attributes with selected partition:
        joint = partition.join(self.attributes, how='inner').drop('partition', axis=1)

        if drop_zero is True:
            # select rows with all zeros values
            return joint.loc[(joint[self.features_name] == 1).any(axis=1)]
        elif 0 <= drop_zero <= 1:
            zero = joint.loc[(joint[self.features_name] == 0).all(axis=1)]
            zero = zero.sample(frac=drop_zero)
            return joint.drop(index=zero.index)

        return joint

def mobilenet_model(hp):
    model = keras.models.Sequential()
    
    model.add(Conv2D(hp.Int("input_units", min_value=32, max_value=256, step=32), (3,3), input_shape = IMG_SHAPE))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    for i in range(hp.Int("n_layers",min_value = 1, max_value = 4, step=1)):
        model.add(Conv2D(hp.Int(f"conv_{i}_units", min_value=32, max_value=256, step=32), (3,3)))
        model.add(Activation('relu'))
    
    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    model.add(Dense(40))
    model.add(Activation("softmax"))
    
    model.compile(optimizer="adam",
                  loss="categorical_crossentropy",
                  metrics=["accuracy"])
    return model

def plots():
    frequencies = celeba.attributes.mean(axis=0).sort_values()

    _ = frequencies.plot(title='CelebA Attributes Frequency',
                         kind='barh',
                         figsize=(12, 12),
                         color='m')
    plt.show()

if __name__ == '__main__':
    
    celeba = CelebA()
    
    train_datagen = ImageDataGenerator(rotation_range=20,
                                       rescale=1. / 255,
                                       width_shift_range=0.2,
                                       height_shift_range=0.2,
                                       shear_range=0.2,
                                       zoom_range=0.2,
                                       horizontal_flip=True,
                                       fill_mode='nearest')
 
    valid_datagen = ImageDataGenerator(rescale=1. / 255)
    
    print('--------Train Generator-----------')
    train_split = celeba.split('0', drop_zero=False)
    valid_split = celeba.split('1', drop_zero=False)

    print('--------Valid Generator-----------')
    train_generator = train_datagen.flow_from_dataframe(
                                        dataframe=train_split,
                                        directory=celeba.images_folder,
                                        x_col='image_id',
                                        y_col=celeba.features_name,
                                        target_size=TARGET_SIZE,
                                        batch_size=1,
                                        class_mode='raw')
    
    print('--------Valid Generator-----------')
    valid_generator = valid_datagen.flow_from_dataframe(
                                        dataframe=valid_split,
                                        directory=celeba.images_folder,
                                        x_col='image_id',
                                        y_col=celeba.features_name,
                                        target_size=TARGET_SIZE,
                                        batch_size=1,
                                        class_mode='raw',
                                        dtype=tf.float32)
    
    
    print('--------Random Search-----------')
    model = RandomSearch(
                        mobilenet_model,
                        objective='val_accuracy',
                        max_trials=5,
                        executions_per_trial=3,
                        directory='my_dir',
                        overwrite=True,
                        project_name='celebA_Tuner')
        
    
    print('--------tunning-----------')
    model.search(train_generator,
                epochs=64,
                batch_size=64,
                steps_per_epoch=len(train_generator),
                validation_data=(train_generator,valid_generator),
                verbose=1
                )
    
    print('--------Best Model---------')
    models = model.get_best_models(num_models=3)
    
    print('--------Model Summary-------')
    model.results_summary()
    
    print('--------finished-----------')

